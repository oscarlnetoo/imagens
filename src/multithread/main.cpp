#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <omp.h>
#include <mpi.h>
#include <iostream>

//precisa escrever a imagem out?
#define WRITE_IMG_OUT	0


//image defines
#define __GROUPSIZE__		5				//tamanho da largura/altura do quadrado para processar a média
#define __STEP__		(__GROUPSIZE__/2)	//quantidade de pixeis para "pular"

//openmpi defines
#define FIRST_TAG	10	//primeira bateria de troca de mensagens
#define SECOND_TAG	20	//segunda bateria de troca de mensagens


using namespace cv;

/**
 * Função que calcula a media dos pixels
 * @param  img Imagem para processar a media
 * @param  x   posicao x do pixel
 * @param  y   posicao y do pixel
 * @return     retorna 3 uchars no formato BGR
 */
void getAverage(Mat* img, int x, int y, unsigned char* result){

	int counter=0;
	int b=0,g=0,r=0; //variáveis auxliares

	for(int i = x - __STEP__; i <= x + __STEP__; i++){
		Vec3b* pixel_array = img->ptr<Vec3b>(i);
		for(int j = y - __STEP__; j <= y + __STEP__; j++){
			if(i < 0 || i >= img->rows || j < 0 || j >= img->cols ){
				//se algo precisa ser feito em caso de ultrapassar margens
			} else {
				b += pixel_array[j][0];
				g += pixel_array[j][1];
				r += pixel_array[j][2]; //acumulando valores
				counter++; //quantos pixels fazem parte da media
			}
		}
	}
	
	result[0] = b / counter; //processando medias
	result[1] = g / counter;
	result[2] = r / counter;
}

/**
 * Faz smooth de uma imagem da linha START ate END
 * @param  origin imagem original
 * @param  start  linha start
 * @param  end    linha end
 * @param  rank   rank do processo, necessário para alocar no processo root
 * @return        retorna a imagem completa
 */
Mat* smoothRGB(Mat *origin, int start, int end, int rank){

	Mat *modified; //nova imagem
	
	//aloca de acordo com o rank
	if(rank == 0){
		modified = new Mat(origin->rows, origin->cols, origin->type());
	} else {
		modified = new Mat(end - start, origin->cols, origin->type());
	}

	//parte paralela do processo que criara threads
	#pragma omp parallel
	{
		//cada thread precisa ter seu buffer
		unsigned char *buffer = (unsigned char*) calloc(3, sizeof(unsigned char));
	
		#pragma omp for
		for (int i = start; i < end; i++){
			//Ponteiro para pixels
			Vec3b* pixel_array = modified->ptr<Vec3b>(i-start);

			//loop de colunas
			for (int j = 0; j < origin->cols; j++){
				 getAverage(origin, i, j, buffer);
				
				//computando media
				pixel_array[j][2] = buffer[2];
				pixel_array[j][1] = buffer[1];
				pixel_array[j][0] = buffer[0];			
			}
		}
	
		free(buffer); //liberando buffer
	
	}	


	//matriz modificada
	return modified;
}

/**
 * Retorna sizeof ocupado pelas linhas
 * @param  cols numero de colunas
 * @return      sizeof da linha
 */
int lineSize(int cols){
	return 3*sizeof(unsigned char)*cols;
}

/**
 * Retorna size de N linhas
 * @param  lines quantidade de linhas
 * @param  cols  quantidade de colunas
 * @return       retorno 
 */
int getRowsSize(int lines, int cols){
	return lines*lineSize(cols);
}

/**
 * Onde comeca? Qual o size do deslocamento? Posso ter meu ponteiro uchar de volta?
 * Todas as suas respostas voce encontra nessa funcao e ligando para o 0800
 * @param  image imagem dos dados
 * @param  start inicio dos dados Y
 * @param  end   final Y
 * @param  size  tamanho do dado
 * @return       ponteiro para start
 */
unsigned char* getChunkStartPointer(Mat* image, int start, int end, int *size){
	int total_size;
	int off_set = getRowsSize(start, image->cols); //offset inicial

	if(end > image->rows) end = image->rows;
	
	total_size = getRowsSize(end - start, image->cols);

	(*size) = total_size;
	
	return (unsigned char*)( image->data + off_set);
}

/**
 * Envia para todos os nos o pedacos de dados
 * @param image      imagem para dados
 * @param n_process  numero de processos disponivel
 * @param chunk_size tamanho do pedaco
 */
void sendChunks(Mat *image, int n_process, int chunk_size){

	unsigned char *send_pointer; //ponteiro para os dados
	int up_border = __STEP__; //borda de cima, precisamos enviar mais dados do q chunk_size
	int low_border = __STEP__; //borda de baixo, no ultimo precisa conter o resto dos pixels
	int y_from, y_to;
	int total_size;

	for (int i = 1; i < n_process; i++){ //loop dos processos
		
		if(i == n_process-1)
			low_border = image->rows%n_process;

		//start e end
		y_from = chunk_size*(i) - up_border;
		y_to = chunk_size*(i+1) + low_border;

		//tamanho total
		total_size = getRowsSize(y_to - y_from, image->cols);

		//enviando o array
		send_pointer = getChunkStartPointer(image, y_from, y_to, &total_size);
		MPI_Send(send_pointer, total_size, MPI_UNSIGNED_CHAR, i, FIRST_TAG ,MPI_COMM_WORLD);
	}
}

/**
 * Recebe todos os pedacos das iamgens dos slaves
 * @param image      imagem
 * @param n_process  numero de process
 * @param chunk_size tamanho do trecho
 */
void receiveChunks(Mat *image, int n_process, int chunk_size){
	
	unsigned char *data_pointer;
	int low_border = 0;
	int y_from, y_to;
	int total_size, data_size;

	for (int i = 1; i < n_process; i++){
		
		if(i == n_process-1){
			//BUG AQUI, o +1 não deveria existir mas se não coloco dá pau
			low_border = (image->rows)%n_process+1;
		}

		//legibilidade aq
		y_from = chunk_size*(i);
		y_to = chunk_size*(i+1) + low_border;
		
		total_size = getRowsSize(chunk_size+low_border, image->cols); 

		data_pointer = getChunkStartPointer(image, y_from, y_to, &data_size);

		MPI_Recv(data_pointer, total_size, MPI_UNSIGNED_CHAR, i, SECOND_TAG ,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}
}

/**
 * Envia um chunk para o ROOT
 * @param image imagem
 */
void sendChunk(Mat *image){
	unsigned char* send_buffer = image->data;
	int total_size = getRowsSize(image->rows, image->cols);
	MPI_Send(send_buffer, total_size, MPI_UNSIGNED_CHAR, 0, SECOND_TAG ,MPI_COMM_WORLD);
}

/**
 * Recebe um pedado do ROOT
 * @param image      imagem
 * @param n_process  numero de processos
 * @param my_rank    rank do processo
 * @param chunk_size tamanho do chunk
 * @param total_rows total de linhas
 * @param type       tipo da imagem
 */
void receiveChunk(Mat &image, int n_process, int my_rank, int chunk_size, int total_rows, int type){

	unsigned char* recv_buffer;
	int total_size;
	int borders = __STEP__*2;

	if(my_rank == n_process - 1){
		borders = (total_rows%n_process) + __STEP__;
	}

	image = Mat(chunk_size+borders, image.cols, type);

	total_size = getRowsSize(image.rows, image.cols);

	recv_buffer = (unsigned char*) malloc(sizeof(unsigned char)*total_size);

	MPI_Recv(recv_buffer, total_size, MPI_UNSIGNED_CHAR, 0, FIRST_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	memcpy(image.data, recv_buffer, total_size);
}

/**
 * Simples funcao para processar imagem
 * @param  image     imagem a ser processada
 * @param  n_process numero de processos
 * @param  rank      rank do processo
 * @return           imagem nova
 */
Mat* processChunk(Mat *image, int n_process, int rank){
	if(rank==0){
		return smoothRGB(image, 0, image->rows - __STEP__, rank);
	}else if(rank == n_process-1){
		return smoothRGB(image, __STEP__, image->rows, rank);
	} else {
		return smoothRGB(image, __STEP__, image->rows - __STEP__, rank);
	}
}

/**
 * Funcao de copia de strin
 * @param  string string a ser copiada
 * @return        copia
 */
char *copyString(char *string){

	char *buffer = (char*) malloc(sizeof(char)*(strlen(string)+1));
	strcpy(buffer, string);

	return buffer;
}

void appendTimeToFile(char *filename, double time){

	FILE* file = fopen(filename, "a+");

	fprintf(file, "%lf\n", time);

	fclose(file);
}

int main(int argc, char **argv){
	
	Mat image; //imagem original ou parte dela
	Mat *image_out; //imagem nova
	int my_rank, n_process; //rank do processo e numero de processos
	int chunk_size; //tamanho do chunk
	int type; //tipo da imagem
	char *name_in, *name_out; //nomes
	
	clock_t start=0, end=0;
	double time;

	if(argc != 3){
		printf("Usage: %s <FILE_IN> <FILE_OUT>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	
	//salvando argumentos
	name_in = copyString(argv[1]);
	name_out = copyString(argv[2]);

	//iniciando MPI
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &n_process);
	
	//ROOT calcula parametros iniciais
	if(my_rank == 0){
		image = imread(name_in);

		start = clock();
		
		type = image.type();
		chunk_size = (image.rows / n_process);
	}

	//enviando constantes
	MPI_Bcast(&type, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&image.cols, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&chunk_size, 1, MPI_INT, 0, MPI_COMM_WORLD);

	//troca de mensagens
	if(my_rank == 0){
		sendChunks(&image, n_process, chunk_size);
	} else {
		receiveChunk(image, n_process, my_rank, chunk_size, image.cols, type);
	}
	
	//processa imagem
	image_out = processChunk(&image, n_process, my_rank);

	//troca de mensagens 2
	if(my_rank != 0){
		sendChunk(image_out);
	} else {
		receiveChunks(image_out, n_process, chunk_size);
		
		end = clock();

		char time_filename[50];
		strcpy(time_filename, name_out);
		strcat(time_filename, "_time_main_mt.txt");

		time = (double) (end - start)/ CLOCKS_PER_SEC;

		appendTimeToFile(time_filename, time);

		if(WRITE_IMG_OUT)
			imwrite(name_out, *image_out);
	}

	//free
	image.release();
	image_out->release();
	delete image_out;

	//finalizando processos
	MPI_Finalize();

	return 0;
}
